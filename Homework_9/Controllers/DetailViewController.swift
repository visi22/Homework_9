
import UIKit

class DetailViewController: UIViewController {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var busynessLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    
    var friend: Friend!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = friend.name
        surnameLabel.text = friend.surname
        ageLabel.text = friend.age
        sexLabel.text = friend.sex
        busynessLabel.text = friend.busyness
        phoneNumberLabel.text = friend.phoneNumber
        mailLabel.text = friend.mail
        
        nameLabel.sizeToFit()
        surnameLabel.sizeToFit()
        ageLabel.sizeToFit()
        sexLabel.sizeToFit()
        busynessLabel.sizeToFit()
        phoneNumberLabel.sizeToFit()
        mailLabel.sizeToFit()
    }

}

