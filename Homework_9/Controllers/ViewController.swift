
import UIKit

class ViewController: UIViewController {
    
    var storage: FriendsStorageProtocol!
    
    @IBOutlet weak var tableView: UITableView!

    
    
    var friends: [FriendsProtocol] = [] {
        didSet {
            storage.save(friends: friends)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        storage = FriendsStorage() as FriendsStorageProtocol
        loadFriends()
        UserDefaults.standard.removeObject(forKey: "friends")
    }
    
    private func loadFriends() {
        friends = storage.load()
    }
    
    
    @IBAction func AddFriend(_ sender: Any) {
        let alertController = UIAlertController(title: "Add Friend", message: "", preferredStyle: .alert)
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Name"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Surname"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Age"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Sex"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Busyness"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Phone Number"
        }
        alertController.addTextField(){ textfield in
            textfield.placeholder = "Mail"
        }
       
              
        let createButton = UIAlertAction(title: "Create", style: .default, handler: { _ in
            guard let name = alertController.textFields?[0].text,
            let surname = alertController.textFields?[1].text,
            let age = alertController.textFields?[2].text,
            let sex = alertController.textFields?[3].text,
            let busyness = alertController.textFields?[4].text,
            let phoneNumber = alertController.textFields?[5].text,
            let Mail = alertController.textFields?[6].text else {
                return
            }
            let friend = Friend(name: name, surname: surname, age: age, sex: sex, busyness: busyness, phoneNumber: phoneNumber, mail: Mail)
            self.friends.append(friend)
            self.tableView.reloadData()
    })
            
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cancelButton)
        alertController.addAction(createButton)
        self.present(alertController, animated: true, completion: nil)
        
    }
}


extension ViewController:  UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        
        configure(cell: &cell, for: indexPath)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let friend = friends[indexPath.row]
        performSegue(withIdentifier: "detailIdentifier", sender: friend)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailIdentifier", let friend = sender as? Friend {
            
            let destController = segue.destination as! DetailViewController
            destController.friend = friend
        }
    }
    
    private func configure(cell: inout UITableViewCell, for indexPath: IndexPath) {
        var configuration = cell.defaultContentConfiguration()
        configuration.text = friends[indexPath.row].name
        configuration.secondaryText = friends[indexPath.row].age
        cell.contentConfiguration = configuration
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let actionDelete = UIContextualAction(style: .destructive, title: "Delete") { _,_,_ in
            self.friends.remove(at: indexPath.row)
            tableView.reloadData()
        }
        let actions = UISwipeActionsConfiguration(actions: [actionDelete])
        return actions
    }
    
}




