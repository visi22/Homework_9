//
//  UserModel.swift
//  Homework_9
//
//  Created by Andrew Kvasha on 26.07.2022.
//

import Foundation

protocol FriendsProtocol {
    var name: String? { get set }
    var surname: String? { get set }
    var age: String? { get set }
    var sex: String? { get set }
    var busyness: String? { get set }
    var phoneNumber: String? { get set }
    var mail: String? { get set }
}

struct Friend: FriendsProtocol {
    var name: String?
    var surname: String?
    var age: String?
    var sex: String?
    var busyness: String?
    var phoneNumber: String?
    var mail: String?
}

protocol FriendsStorageProtocol {
    
    func load() -> [FriendsProtocol]
    func save(friends: [FriendsProtocol])
}

class FriendsStorage: FriendsStorageProtocol  {
    
    private var storage = UserDefaults.standard
    var storageKey: String = "friends"
    
    private enum FriendsKey: String{
        case name
        case surname
        case age
        case sex
        case busyness
        case phoneNumber
        case mail
    }
    
    func load() -> [FriendsProtocol] {
        var resultFriends: [FriendsProtocol] = []
        let friendsFromStorage = storage.array(forKey: storageKey) as? [[String:String]] ?? []
        for friend in friendsFromStorage {
            guard let name = friend[FriendsKey.name.rawValue],
                  let surname = friend[FriendsKey.surname.rawValue],
                  let age = friend[FriendsKey.age.rawValue],
                  let sex = friend[FriendsKey.sex.rawValue],
                  let busyness = friend[FriendsKey.busyness.rawValue],
                  let phoneNumber = friend[FriendsKey.phoneNumber.rawValue],
                  let mail = friend[FriendsKey.mail.rawValue] else {
                continue
            }
            resultFriends.append(Friend(name: name, surname: surname, age: age, sex: sex, busyness: busyness, phoneNumber: phoneNumber, mail: mail))
        }
        return resultFriends
    }
    
    func save(friends: [FriendsProtocol]) {
        var arrayForStorage: [[String:String]] = []
        friends.forEach { friend in
            var newElementForStorage: Dictionary<String,String> = [:]
            newElementForStorage[FriendsKey.name.rawValue] = friend.name
            newElementForStorage[FriendsKey.surname.rawValue] = friend.surname
            newElementForStorage[FriendsKey.age.rawValue] = friend.age
            newElementForStorage[FriendsKey.sex.rawValue] = friend.sex
            newElementForStorage[FriendsKey.busyness.rawValue] = friend.busyness
            newElementForStorage[FriendsKey.phoneNumber.rawValue] = friend.phoneNumber
            newElementForStorage[FriendsKey.mail.rawValue] = friend.mail
            arrayForStorage.append(newElementForStorage)
        }
        storage.set(arrayForStorage, forKey: storageKey)
    }
}
